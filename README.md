# Quarkus Task (2024-02)

Gets GitHub user repositories (excluding forks) with their branches.
Written in [Kotlin](https://kotlinlang.org/), uses [Quarkus](https://quarkus.io/).

## Example response

```shell script
curl -s http://localhost:8080/github/notforks/torvalds | jq
```

```json5
[
  {
    "owner": "torvalds",
    "name": "linux",
    "branches": [
      {
        "name": "dependabot/pip/drivers/gpu/drm/ci/xfails/pip-23.3",
        "last_commit_sha": "d566ec94e63702a1bf83f43804b021a8f478a27b"
      },
      {
        "name": "master",
        "last_commit_sha": "b401b621758e46812da61fa58a67c3fd8d91de0d"
      }
    ]
  },
  // truncated...
]
```

## Run dev server (with background compilation and hot deployment)

```shell script
./gradlew quarkusDev
```

## Run tests

```shell script
./gradlew test
```

## Build a native 64-bit Linux executable (with [GraalVM](https://www.graalvm.org/))

```shell script
./gradlew build -Dquarkus.package.type=native -Dquarkus.native.container-build=true -Dquarkus.native.native-image-xmx=4G
```

## API tokens

GitHub REST API allows only a few unauthorized requests per hour.
To get a bigger quota, [generate a new personal access token](https://github.com/settings/personal-access-tokens/new) and provide it as a `github.token` property
\(as a `-D` system property, in an environment variable, or in [any other way that Quarkus allows you to](https://quarkus.io/guides/config-reference#configuration-sources)\).

> [!IMPORTANT]
> You have to grant at least `Contents` and `Metadata` repository permissions.

> [!NOTE]
> Github App installation access tokens can also be used for API authorization.  
> However, such tokens have to be reissued every hour.
> Since their management is non-trivial (signing JWTs etc.) I've decided not to implement it in this sample app.
