package xyz.lukasz.atipera.task

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.runtime.annotations.RegisterForReflection
import io.smallrye.mutiny.coroutines.awaitSuspending
import jakarta.inject.Inject
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.rest.client.inject.RestClient

@Path("/github")
class GithubResource {

    @Inject lateinit var objectMapper: ObjectMapper
    @RestClient lateinit var githubService: GithubService

    @GET
    @Path("/notforks/{user}")
    @Produces(MediaType.APPLICATION_JSON)
    suspend fun listNotForks(@PathParam("user") username: String): Response {

        val knownRepos = when (val result = getUserRepos(username)) {
            is Result.Success -> result.value
            is Result.Failure -> return result.failureResponse
        }

        val invalidRepos = mutableSetOf<NodeId>()
        for ((nodeId, repo) in knownRepos) {
            when (val result = getRepoBranches(repo.owner, repo.name)) {
                is Result.Success -> when (val branches = result.value) {
                    null -> invalidRepos.add(nodeId)
                    else -> repo.branches.addAll(branches.values.sortedBy { it.name })
                }
                is Result.Failure -> return result.failureResponse
            }
        }

        invalidRepos.forEach { knownRepos.remove(it) }
        return Response.ok(knownRepos.values.sortedBy { it.name }).build()
    }

    private suspend fun getUserRepos(
        username: String,
    ): Result<MutableMap<NodeId, RepoDetailsDto>> {

        val knownRepos = mutableMapOf<NodeId, RepoDetailsDto>()
        var page = 1

        while (true) {

            val response = try {
                githubService.getUserReposAsUni(username, page).awaitSuspending()
            } catch (e: GithubService.RateLimitedException) {
                return Result.Failure(Response.status(429)
                    .entity(ErrorDetailsDto(429, e.humanizedDetails()))
                    .build())
            } catch (_: NotFoundException) {
                return Result.Failure(Response.status(404)
                    .entity(ErrorDetailsDto(404, "No such GitHub user exists"))
                    .build())
            } catch (e: WebApplicationException) {
                return Result.Failure(Response.status(e.response.status)
                    .entity(ErrorDetailsDto(e.response.status, "GitHub could not fulfill the request"))
                    .build())
            }

            val details = try {
                val body = response.readEntity(String::class.java)
                objectMapper.readValue(body, object : TypeReference<Array<GithubService.RepoDetails>>() {})
            } catch (e: Throwable) {
                return Result.Failure(Response.status(500)
                    .entity(ErrorDetailsDto(500, "Cannot read GitHub response: $e"))
                    .build())
            }

            for (repo in details.filterNot { it.fork }) {
                knownRepos[repo.nodeId] = RepoDetailsDto(
                    owner = repo.owner.login,
                    name = repo.name,
                    branches = mutableListOf(),
                )
            }

            if (!response.hasLinkToNextPage()) {
                break
            }

            page += 1
        }

        return Result.Success(knownRepos)
    }

    private suspend fun getRepoBranches(
        owner: String,
        repo: String,
    ): Result<Map<String, BranchDetailsDto>?> {

        val knownBranches = mutableMapOf<String, BranchDetailsDto>()
        var page = 1

        while (true) {

            val response = try {
                githubService.getRepoBranchDetailsAsUni(owner, repo, page).awaitSuspending()
            } catch (e: GithubService.RateLimitedException) {
                return Result.Failure(Response.status(429)
                    .entity(ErrorDetailsDto(429, e.humanizedDetails()))
                    .build())
            } catch (_: NotFoundException) {
                // The user must have privated/deleted/moved the repo between our API calls.
                // As such, we do not want to list it in our response
                return Result.Success(null)
            } catch (e: WebApplicationException) {
                return Result.Failure(Response.status(e.response.status)
                    .entity(ErrorDetailsDto(e.response.status, "GitHub could not fulfill the request"))
                    .build())
            }

            val details = try {
                val body = response.readEntity(String::class.java)
                objectMapper.readValue(body, object : TypeReference<Array<GithubService.BranchDetails>>() {})
            } catch (e: Throwable) {
                return Result.Failure(Response.status(500)
                    .entity(ErrorDetailsDto(500, "Cannot read GitHub response: $e"))
                    .build())
            }

            for (branch in details) {
                knownBranches[branch.name] = BranchDetailsDto(
                    name = branch.name,
                    lastCommitSha = branch.commit.sha,
                )
            }

            if (!response.hasLinkToNextPage()) {
                break
            }

            page += 1
        }

        return Result.Success(knownBranches)
    }

    /**
     * Checks if a paginated GitHub API response
     * has a link to another page in its header.
     */
    private fun Response.hasLinkToNextPage(): Boolean {
        val headerLink = this.stringHeaders.getFirst("link") ?: return false
        return headerLink.contains("; rel=\"next\"")
    }

    @RegisterForReflection
    data class BranchDetailsDto(
        @field:JsonProperty("name") val name: String,
        @field:JsonProperty("last_commit_sha") val lastCommitSha: String,
    )

    @RegisterForReflection
    data class RepoDetailsDto(
        @field:JsonProperty("owner") val owner: String,
        @field:JsonProperty("name") val name: String,
        @field:JsonProperty("branches") val branches: MutableList<BranchDetailsDto>,
    )

    @RegisterForReflection
    data class ErrorDetailsDto(
        @field:JsonProperty("status") val status: Int,
        @field:JsonProperty("message") val message: String,
    )
}