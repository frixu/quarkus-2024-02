package xyz.lukasz.atipera.task

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import io.quarkus.rest.client.reactive.ClientExceptionMapper
import io.quarkus.rest.client.reactive.ClientQueryParam
import io.quarkus.runtime.annotations.RegisterForReflection
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.ext.Provider
import org.apache.commons.lang3.time.DurationFormatUtils
import org.eclipse.microprofile.config.ConfigProvider
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestContext
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestFilter
import java.time.Duration
import java.time.Instant

typealias NodeId = String

@RegisterRestClient(configKey = "github-rest-api")
@RegisterProvider(GithubService.AuthRequestFilter::class)
@ClientHeaderParam(name = "User-Agent", value = ["atipera-task-app"])
@ClientHeaderParam(name = "X-GitHub-Api-Version", value = ["2022-11-28"])
interface GithubService {

    @GET
    @Path("/users/{user}/repos")
    @ClientQueryParam(name = "per_page", value = ["100"])
    fun getUserReposAsUni(
        @PathParam("user") userName: String,
        @QueryParam("page") page: Int = 1,
    ): Uni<Response>

    @GET
    @Path("/repos/{user}/{repo}/branches")
    @ClientQueryParam(name = "per_page", value = ["100"])
    fun getRepoBranchDetailsAsUni(
        @PathParam("user") userName: String,
        @PathParam("repo") repoName: String,
        @QueryParam("page") page: Int = 1,
    ): Uni<Response>

    @RegisterForReflection
    data class UserDetails @JsonCreator constructor(
        @field:JsonProperty("login") val login: String,
    )

    @RegisterForReflection
    data class RepoDetails @JsonCreator constructor(
        @field:JsonProperty("name") val name: String,
        @field:JsonProperty("node_id") val nodeId: NodeId,
        @field:JsonProperty("fork") val fork: Boolean,
        @field:JsonProperty("owner") val owner: UserDetails,
    )

    @RegisterForReflection
    data class CommitDetails @JsonCreator constructor(
        @field:JsonProperty("sha") val sha: String,
    )

    @RegisterForReflection
    data class BranchDetails @JsonCreator constructor(
        @field:JsonProperty("name") val name: String,
        @field:JsonProperty("commit") val commit: CommitDetails,
    )

    companion object {
        @JvmStatic
        @ClientExceptionMapper(priority = -100)
        fun rateLimitedToException(response: Response): RuntimeException? {

            val headers = response.stringHeaders
            val requestsRemaining = headers.getFirst("x-ratelimit-remaining")?.toIntOrNull()
            if (requestsRemaining != 0) {
                return null
            }

            val limitResetTime = headers.getFirst("x-ratelimit-reset")
                ?.toLongOrNull()
                ?.let { Instant.ofEpochSecond(it) }

            return RateLimitedException(limitResetTime)
        }
    }

    class RateLimitedException(
        private val resetTime: Instant?
    ) : RuntimeException("Rate-limited by GitHub") {

        fun humanizedDetails(): String {
            return "${message}. Try again ${
                if (resetTime == null) {
                    "later"
                } else {
                    "in ${DurationFormatUtils.formatDurationWords(
                        Duration.between(Instant.now(), resetTime).toMillis(),
                        true,
                        true
                    )}"
                }
            }"
        }
    }

    @Provider
    class AuthRequestFilter : ResteasyReactiveClientRequestFilter {
        override fun filter(requestContext: ResteasyReactiveClientRequestContext) {
            val config = ConfigProvider.getConfig()
            val token = config.getConfigValue("github.token").value ?: return
            requestContext.headers.add(AUTHORIZATION, "Bearer $token")
        }
    }
}
