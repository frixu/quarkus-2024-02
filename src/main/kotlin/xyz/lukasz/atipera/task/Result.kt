package xyz.lukasz.atipera.task

import jakarta.ws.rs.core.Response

/**
 * A result of some operation –
 * either a `Success` with a value,
 * or a `Failure` with a built `Response` object to be sent back to the client.
 */
sealed class Result<T> private constructor() {
    class Success<T>(val value: T): Result<T>()
    class Failure<T>(val failureResponse: Response): Result<T>()
}