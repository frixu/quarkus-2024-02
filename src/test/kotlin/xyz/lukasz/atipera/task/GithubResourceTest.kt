package xyz.lukasz.atipera.task

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType.HTML
import io.restassured.http.ContentType.JSON
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.isA
import org.hamcrest.Matchers.aMapWithSize
import org.hamcrest.Matchers.empty
import org.jboss.resteasy.reactive.RestResponse.StatusCode.*
import org.junit.jupiter.api.Test
import java.time.Instant

@QuarkusTest
@TestHTTPEndpoint(GithubResource::class)
@QuarkusTestResource(GithubResourceTest.WireMockTestResource::class)
class GithubResourceTest {

    @Test
    fun `Endpoint only produces JSON`() {
        given()
            .accept(HTML)
            .get("/notforks/any-user-will-do")
            .then()
                .statusCode(NOT_ACCEPTABLE)
    }

    @Test
    fun `Endpoint correctly reports non-existing user`() {
        given()
            .accept(JSON)
            .get("/notforks/this-user-does-not-exist")
            .then()
                .statusCode(NOT_FOUND)
                .contentType(JSON)
                .body("", aMapWithSize<String, Any>(2))
                .body("status", `is`(404))
                .body("message", isA(String::class.java))
    }

    @Test
    fun `Endpoint correctly reports user with no repositories`() {
        given()
            .accept(JSON)
            .get("/notforks/this-user-has-no-repos")
            .then()
                .statusCode(OK)
                .contentType(JSON)
                .body("", empty<Any>())
    }

    @Test
    fun `Endpoint correctly fetches multiple repo pages`() {
        given()
            .accept(JSON)
            .get("/notforks/this-user-has-two-repos")
            .then()
                .statusCode(OK)
                .contentType(JSON)
                .body("$.size()", `is`(2))
    }

    @Test
    fun `Endpoint handles rate limit when fetching user`() {
        given()
            .accept(JSON)
            .get("/notforks/this-user-is-rate-limited")
            .then()
                .statusCode(TOO_MANY_REQUESTS)
    }

    class WireMockTestResource : QuarkusTestResourceLifecycleManager {

        private var mockServer: WireMockServer? = null

        override fun start(): Map<String, String> {

            val mockServer = WireMockServer(wireMockConfig().dynamicPort())
            mockServer.start()

            mockServer.stubFor(
                get(urlPathEqualTo("/users/this-user-does-not-exist/repos"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withStatus(NOT_FOUND)
                            .withBody("""{"message": "Not Found", "documentation_url": "https://example.com"}""")
                    )
            )

            mockServer.stubFor(
                get(urlPathEqualTo("/users/this-user-has-no-repos/repos"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withStatus(OK) // Yes, 200, NOT 204
                            .withBody("[ ]")
                    )
            )

            mockServer.stubFor(
                get(urlPathEqualTo("/users/this-user-has-two-repos/repos"))
                    .withQueryParam("page", equalTo("1"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withHeader("link",
                                "<${mockServer.baseUrl()}/user/this-user-has-two-repos/repos?page=2>; rel=\"next\", " +
                                        "<$${mockServer.baseUrl()}/user/this-user-has-two-repos/repos?page=2>; rel=\"last\"")
                            .withStatus(OK)
                            .withBody("""[
                                {
                                    "id": 1,
                                    "node_id": "MDEwOlJlcG9zaXRvcnkx",
                                    "name": "Foo",
                                    "full_name": "this-user-has-two-repos/Foo",
                                    "owner": {
                                        "login": "this-user-has-two-repos",
                                        "id": 10,
                                        "node_id": "MDQ6VXNlcjEw"
                                    },
                                    "description": null,
                                    "fork": false,
                                    "url": "${mockServer.baseUrl()}/repos/this-user-has-two-repos/Foo",
                                    "branches_url": "${mockServer.baseUrl()}/repos/this-user-has-two-repos/Foo/branches{/branch}",
                                    "forks_count": 0,
                                    "forks": 0
                                }
                            ]""")
                    )
            )

            mockServer.stubFor(
                get(urlPathEqualTo("/repos/this-user-has-two-repos/Foo/branches"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withStatus(OK)
                            .withBody("""[
                                {
                                    "name": "master",
                                    "commit": {
                                        "sha": "0000000000000000000000000000000000000000",
                                        "url": "example.com"
                                    },
                                    "protected": false
                                }
                            ]""")
                    )
            )

            mockServer.stubFor(
                get(urlPathEqualTo("/users/this-user-has-two-repos/repos"))
                    .withQueryParam("page", equalTo("2"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withHeader("link",
                                "<${mockServer.baseUrl()}/user/this-user-has-two-repos/repos?page=1>; rel=\"prev\", " +
                                        "<$${mockServer.baseUrl()}/user/this-user-has-two-repos/repos?page=1>; rel=\"first\"")
                            .withStatus(OK)
                            .withBody("""[
                                {
                                    "id": 2,
                                    "node_id": "MDEwOlJlcG9zaXRvcnky",
                                    "name": "Bar",
                                    "full_name": "this-user-has-two-repos/Bar",
                                    "owner": {
                                        "login": "this-user-has-two-repos",
                                        "id": 10,
                                        "node_id": "MDQ6VXNlcjEw"
                                    },
                                    "description": null,
                                    "fork": false,
                                    "url": "${mockServer.baseUrl()}/repos/this-user-has-two-repos/Bar",
                                    "branches_url": "${mockServer.baseUrl()}/repos/this-user-has-two-repos/Bar/branches{/branch}",
                                    "forks_count": 0,
                                    "forks": 0
                                }
                            ]""")
                    )
            )

            mockServer.stubFor(
                get(urlPathEqualTo("/repos/this-user-has-two-repos/Bar/branches"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withStatus(OK)
                            .withBody("""[
                                {
                                    "name": "main",
                                    "commit": {
                                        "sha": "0000000000000000000000000000000000000001",
                                        "url": "example.com"
                                    },
                                    "protected": false
                                }
                            ]""")
                    )
            )

            mockServer.stubFor(
                get(urlPathEqualTo("/users/this-user-is-rate-limited/repos"))
                    .willReturn(
                        aResponse()
                            .withHeader("content-type", "application/json; charset=utf-8")
                            .withHeader("x-ratelimit-limit", "60")
                            .withHeader("x-ratelimit-used", "60")
                            .withHeader("x-ratelimit-remaining", "0")
                            .withHeader("x-ratelimit-resource", "core")
                            .withHeader("x-ratelimit-reset", Instant.now().plusSeconds(10).epochSecond.toString())
                            .withStatus(FORBIDDEN) // Yes, 403, NOT 429
                            .withBody("""{"message": "API rate limit [...]", "documentation_url": "https://example.com"}""")
                    )
            )

            this.mockServer = mockServer
            return mapOf(
                "quarkus.rest-client.github-rest-api.url" to mockServer.baseUrl(),
            )
        }

        override fun stop() {
            mockServer?.stop()
        }
    }
}